//3) Create a variable getCube and use the exponent operator to compute for the cube of a number. 
//4)Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is.
    let num = (2)
	let getCube = (num ** 3);
	//console.log(getCube)

	console.log(`The cube of ${num} is ${getCube}`)
//5) Create a variable address with a value of an array containing details of an address.
//6)Destructure the array and print out a message with the full address using Template Literals.

	let address = ["258", "Washington Ave", "NW", "California", "90011"];
	//console.log(address)
	function currentAdd (arrAdd){

	let [street, avenue, city, county, zipCode] = arrAdd

	return `I live at ${street} ${avenue} ${city}, ${county} ${zipCode}.` }
	console.log(currentAdd(address));

//7) Create a variable animal with a value of an object data type with different animal details as it's properties.
	let animal = {
		name: "Lolong",
		type: "saltwater crodile",
		weight: "1075 kg",
		length: "20 ft 3 in"
		};

	
//8)Destructure the object and print out a message with the details of the animal using Template Literals.


	let {name, type, weight, length} = animal
	console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`)

//9) Create an array of numbers.
let arrayNumbers = [1, 2, 3, 4, 5];
//10) Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
arrayNumbers.forEach(numbers => console.log(numbers));
//11- Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
let reduceNumber = arrayNumbers.reduce((a,b) => a + b)
	console.log(reduceNumber);
//12- Create a class of a Dog and a constructor that will accept a name, age and breed as it's properties.
class dog{
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}
//13- Create/instantiate a new object from the class Dog and console log the object.
let doggo = new dog("Frankie", 5, "Miniature Dachshund");
	console.log(doggo);
